// eslint-disable-next-line
import { createElement } from './helper.mjs';

export const createRoomCard = (socket, { id, users }) => {
  const roomCard = createElement({
    tagName: 'div',
    className: 'room-card no-select',
    attributes: { id }
  });

  const connectedUsers = createElement({
    tagName: 'span',
    className: 'room-users-count no-select'
  });

  const usersTextPlural = users.length === 1 ? 'user' : 'users';

  connectedUsers.innerText = `${users.length} ${usersTextPlural} connected`;
  roomCard.append(connectedUsers);

  const roomName = createElement({
    tagName: 'span',
    className: 'room-name no-select'
  });
  roomName.innerText = id;
  roomCard.append(roomName);

  const joinButton = createElement({
    tagName: 'button',
    className: 'ui-button no-select'
  });

  joinButton.innerText = 'Join';
  roomCard.append(joinButton);

  const onJoinRoom = () => {
    socket.emit('JOIN_ROOM', id);
  };

  joinButton.addEventListener('click', onJoinRoom);

  return roomCard;
};
