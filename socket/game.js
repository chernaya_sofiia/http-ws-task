import { MAXIMUM_USERS_FOR_ONE_ROOM, SECONDS_TIMER_BEFORE_START_GAME, SECONDS_FOR_GAME } from './config';
import { texts } from '../database/data';
import { userLogin, findByUsername, userDisconnected } from '../database/users';
import {
  getActiveRooms,
  getRoomById,
  createRoom,
  addUserToRoom,
  removeUserFromRoom,
  deleteRoom,
  setRoomFull,
  toggleUserReady,
  resetRoomStatus,
  setStartGame,
  updateGameProgress,
  setUserFinished
} from '../database/rooms';

const timers = new Map();

const updateActiveRooms = socket => {
  const rooms = getActiveRooms();
  socket.broadcast.emit('UPDATE_ROOMS', rooms);
  socket.emit('UPDATE_ROOMS', rooms);
};
const checkGameEnd = roomId => {
  const room = getRoomById(roomId);
  return room?.users.every(user => user.finished === true);
};

const gameOver = (roomId, socket) => {
  resetRoomStatus(roomId);
  clearInterval(timers.get(roomId));
  const room = getRoomById(roomId);
  const sortedUsers = room.users.sort((a, b) => b.progress - a.progress);
  socket.to(roomId).emit('GAME_ENDED', sortedUsers);
  socket.emit('GAME_ENDED', sortedUsers);
  updateActiveRooms(socket);
};

const untilEndCountdown = (roomId, socket) => {
  let timeLeft = SECONDS_FOR_GAME;
  const timeBeforeEnd = setInterval(() => {
    timeLeft -= 1;
    socket.to(roomId).emit('TIME_UNTIL_END', timeLeft);
    socket.emit('TIME_UNTIL_END', timeLeft);

    if (timeLeft === 0) {
      clearInterval(timeBeforeEnd);
      gameOver(roomId, socket);
    }
  }, 1000);
  timers.set(roomId, timeBeforeEnd);
};

const startCountdown = (roomId, socket) => {
  const randomTextIindex = Math.floor(Math.random() * texts.length);
  const toSend = { id: randomTextIindex };
  socket.to(roomId).emit('GAME_COUNTDOWN_START', toSend);
  socket.emit('GAME_COUNTDOWN_START', toSend);
  let timeLeft = SECONDS_TIMER_BEFORE_START_GAME;
  const timeBeforeStart = setInterval(() => {
    timeLeft -= 1;
    socket.to(roomId).emit('TIME_UNTIL_START', timeLeft);
    socket.emit('TIME_UNTIL_START', timeLeft);

    if (timeLeft === 0) {
      clearInterval(timeBeforeStart);
      untilEndCountdown(roomId, socket);
    }
  }, 1000);
  timers.set(roomId, timeBeforeStart);
};

const leaveRoomHelper = (roomId, socket) => {
  removeUserFromRoom(roomId, socket.id);
  const room = getRoomById(roomId);
  if (!room?.users.length) {
    clearInterval(timers.get(roomId));
    deleteRoom(roomId);
  } else {
    setRoomFull(roomId, false);
    socket.to(roomId).emit('UPDATE_ROOM', room);
    // start game if someone left, but everyone else ready
    if (room.users.every(user => user.ready) && !room.gameStarted) {
      setStartGame(roomId, true);
      startCountdown(roomId, socket);
    }
    if (checkGameEnd(roomId)) {
      gameOver(roomId, socket);
    }
  }
  updateActiveRooms(socket);
};

const leaveRoom = (roomId, socket) => {
  socket.leave(roomId, () => {
    leaveRoomHelper(roomId, socket);
    socket.emit('LEAVE_ROOM_DONE');
  });
};

const joinRoom = (roomId, socket) => {
  socket.join(roomId, () => {
    const room = addUserToRoom(roomId, socket.id);
    socket.emit('JOIN_ROOM_DONE', room);
    if (room?.users?.length === MAXIMUM_USERS_FOR_ONE_ROOM) {
      setRoomFull(room.id, true);
    }
    socket.to(roomId).emit('UPDATE_ROOM', room);
    updateActiveRooms(socket);
  });
};

const createNewRoom = (roomId, socket) => {
  if (getRoomById(roomId)) {
    socket.emit('ROOM_EXISTS');
    return;
  }
  createRoom(roomId, socket.id);
  joinRoom(roomId, socket);
  updateActiveRooms(socket);
};

const updateRoom = (roomId, socket) => {
  const room = toggleUserReady(roomId, socket.id);
  if (room) {
    socket.to(roomId).emit('UPDATE_ROOM', room);
    socket.emit('UPDATE_ROOM', room);
    if (room.users.every(user => user.ready)) {
      setStartGame(roomId, true);
      startCountdown(roomId, socket);
      updateActiveRooms(socket);
    }
  }
};

export const getGameText = textId => {
  const text = texts[textId];
  return text;
};

const updateRoomProgress = (progress, roomId, socket) => {
  const room = updateGameProgress(socket.id, roomId, progress);
  socket.to(roomId).emit('UPDATE_ROOM', room);
  socket.emit('UPDATE_ROOM', room);
};

export default io => {
  io.on('connection', socket => {
    const { handshake: { query: { username } } } = socket;
    socket.on('disconnecting', () => {
      const rooms = Object.keys(socket.rooms);
      rooms.forEach(roomId => {
        leaveRoomHelper(roomId, socket);
      });
    });

    socket.on('disconnect', () => {
      userDisconnected(socket.id);
    });

    if (findByUsername(username)) {
      socket.emit('USER_EXISTS');
    } else {
      userLogin(socket.id, username);
    }

    socket.emit('UPDATE_ROOMS', getActiveRooms());
    socket.on('JOIN_ROOM', roomId => joinRoom(roomId, socket));
    socket.on('LEAVE_ROOM', roomId => leaveRoom(roomId, socket));
    socket.on('CREATE_NEW_ROOM', roomId => createNewRoom(roomId, socket));
    socket.on('USER_READY', roomId => updateRoom(roomId, socket));
    socket.on('GAME_PROGRESS', ({ percent, roomId }) => updateRoomProgress(percent, roomId, socket));
    socket.on('USER_FINISHED', roomId => {
      setUserFinished(roomId, socket.id);
      if (checkGameEnd(roomId)) {
        gameOver(roomId, socket);
      }
    });
  });
};
